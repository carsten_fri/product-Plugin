# Product Plugin

Felder/Eigenschaften (sofern Standard, WP-Standard verwenden)
https://developers.google.com/search/docs/data-types/product#structured-data-type-definitions

    Titel -> WP Titel Standard (kein ACF)
    Beschreibung (ACF)
    Bild (WP Thumbnail)
    SKU (ACF)
    MPN (ACF)
    Marke (Eventuell als Kategorie?) -> (WP Kategorie)
    Preis + Streichpreis (ACF)

Ablauf bei Installation:
- ACF soll installiert werden, falls nicht vorhanden
- Custom Post Type soll vorhanden sein
- ACF Felder sollen erstellt sein
